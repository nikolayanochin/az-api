# az-api-class
As base client for requests use Guzzle5. You can read guzzle6 documentation for more opportunity. 
#
https://az.express/api/docs -- main documentation
https://az.express/ -- contacts 
#
```php
use AzExpressTeam\Api;

$api = new Api(['secret_key' => '5f53334da9f84e43b67425ef5gggad23']);

$client_uid = 'TEST000000003';
try {
    // if your request may have clients error as (4xx) use try-catch
    // Create invoice
    $request = $api->invoice()->create([
        'city' => 'Moscow',
        'contact' => 'Test contact name',
        'phone' => '+71112223344',
        'postcode' => '12345',
        'address' => 'Test street name 123',
        'eid' => $client_uid,
        'weight' => 0.5,
    ]);
    $invoice = json_decode($request->getBody());


    // List invoices
    $request = $api->invoice()->getList([
        'ids' => "{$client_uid},TEST000000002"
    ]);
    $invoices = json_decode($request->getBody());


    // Update invoice
    $request = $api->invoice()->update($client_uid, [
        'contact' => 'Test contact name2',
    ]);
    $invoice = json_decode($request->getBody());


    // View invoice
    $request = $api->invoice()->getView($client_uid);
    $invoice = json_decode($request->getBody());


    // Status history invoice
    $request = $api->invoice()->statusHistory($client_uid);
    $invoiceStatusHistory = json_decode($request->getBody());

    // Calculation invoice cost
    $request = $api->invoice()->cost(['weight' => $invoice->weight ?? 0.5]);
    $invoiceCost = json_decode($request->getBody());


    // Delete invoice
    $request = $api->invoice()->delete($client_uid);


    // Invoice status list
    $request = $api->dictionary()->getStatusesList();
    $statuses = json_decode($request->getBody());

    // Invoice pdf sticker in binary format
    $request = $api->invoice()->getPdfSticker($client_uid);
    $binaryFile = $request->getBody();


    // Create manifest
    $request = $api->manifest()->create([
        'type' => \AzExpressTeam\Actions\Manifest::TYPE_COMING,
        'stamp' => 'TEST_MANIFEST_STAMP_1',
        'date' => date('Y-m-d'),
        'invoice_ids' => [
            $client_uid
        ]
    ]);
    $manifest = json_decode($request->getBody());


    // List manifests
    $request = $api->manifest()->getList([
        'date_from' => "2019-01-01",
        'date_to' => date('Y-m-d'),
    ]);
    $manifests = json_decode($request->getBody());


    // Update manifest
    $request = $api->manifest()->update($manifest->id, [
        'stamp' => 'TEST_MANIFEST_STAMP_2',
    ]);
    $manifest = json_decode($request->getBody());


    // View manifest
    $request = $api->manifest()->getView($manifest->id);
    $manifest = json_decode($request->getBody());


    // Delete manifest
    $request = $api->manifest()->delete($manifest->id);


} catch (\GuzzleHttp\Exception\ClientException $e) {
    $response = $e->getResponse();
    $responseBodyAsString = $response->getBody()->getContents();
    $error = json_decode($responseBodyAsString);
} catch (\GuzzleHttp\Exception\ServerException $e) {
    // More information in guzzle v5 documentation
}
```