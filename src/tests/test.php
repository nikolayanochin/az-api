<?php

require_once __DIR__ . '/../../vendor/autoload.php'; // Autoload files using Composer autoload

use AzExpressTeam\Api;

$api = new Api(['secret_key' => '5f53334da9f84e43b67425ef5gggad23']);

$client_uid = 'TEST000000003';
try {
    // if your request may have clients error as (4xx) use try-catch
    // Create invoice
    $request = $api->invoice()->create([
        'address' => "ул Софьи Ковалевской, д 2, кв 102",
        'address_from' => "ул Пушкина дом Калатушкина",
        'amount' => 2,
        'client_dimensions_height' => 1,
        'client_dimensions_length' => 3,
        'client_dimensions_width' => 3,
        'client_uid' => "AZT2000000043",
        'comment' => "12345678",
        'company_from_name' => "КОМПАНИЯ 1",
        'company_to_name' => "КОМПАНИЯ 2",
        'contact' => "Я есть получатель",
        'contact_from' => "Я есть отправитель",
        'declared_weight' => 630,
        'phone' => "89251742356",
        'phone_from' => "89251742356",
        'tariff' => 1,
    ]);
    $invoice = json_decode($request->getBody());


    // List invoices
    $request = $api->invoice()->getList([
        'ids' => "{$client_uid},TEST000000002"
    ]);
    $invoices = json_decode($request->getBody());


    // Update invoice
    $request = $api->invoice()->update($client_uid, [
        'contact' => 'Test contact name2',
    ]);
    $invoice = json_decode($request->getBody());


    // View invoice
    $request = $api->invoice()->getView($client_uid);
    $invoice = json_decode($request->getBody());


    // Status history invoice
    $request = $api->invoice()->statusHistory($client_uid);
    $invoiceStatusHistory = json_decode($request->getBody());

    // Calculation invoice cost
    $request = $api->invoice()->cost(['weight' => $invoice->weight ?? 0.5]);
    $invoiceCost = json_decode($request->getBody());


    // Delete invoice
    $request = $api->invoice()->delete($client_uid);


    // Invoice status list
    $request = $api->dictionary()->getStatusesList();
    $statuses = json_decode($request->getBody());

    // Invoice pdf sticker in binary format
    $request = $api->invoice()->getPdfSticker($client_uid);
    $binaryFile = $request->getBody();


    // Create manifest
    $request = $api->manifest()->create([
        'type' => \AzExpressTeam\Actions\Manifest::TYPE_COMING,
        'stamp' => 'TEST_MANIFEST_STAMP_1',
        'date' => date('Y-m-d'),
        'invoice_ids' => [
            $client_uid
        ]
    ]);
    $manifest = json_decode($request->getBody());


    // List manifests
    $request = $api->manifest()->getList([
        'date_from' => "2019-01-01",
        'date_to' => date('Y-m-d'),
    ]);
    $manifests = json_decode($request->getBody());


    // Update manifest
    $request = $api->manifest()->update($manifest->id, [
        'stamp' => 'TEST_MANIFEST_STAMP_2',
    ]);
    $manifest = json_decode($request->getBody());


    // View manifest
    $request = $api->manifest()->getView($manifest->id);
    $manifest = json_decode($request->getBody());


    // Delete manifest
    $request = $api->manifest()->delete($manifest->id);


} catch (\GuzzleHttp\Exception\ClientException $e) {
    $response = $e->getResponse();
    $responseBodyAsString = $response->getBody()->getContents();
    $error = json_decode($responseBodyAsString);
} catch (\GuzzleHttp\Exception\ServerException $e) {
    // More information in guzzle v5 documentation
}

