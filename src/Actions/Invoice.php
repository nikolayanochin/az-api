<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-02-19
 */

namespace AzExpressTeam\Actions;

use GuzzleHttp\Client;

/**
 * Class Invoice
 * @package AzExpressTeam\Actions
 */
class Invoice extends BaseAction
{
    /** @var Client */
    protected $client;

    /**
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \ReflectionException
     */
    public function getList($params = [])
    {
        $query = $this->getSearchString($params);
        return $this->client->get("invoices{$query}");
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getView($id)
    {
        return $this->client->get("invoices/{$id}");
    }

    /**
     * @param $id
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update($id, $body)
    {
        return $this->client->put("invoices/{$id}", ['form_params' => $body]);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($id)
    {
        return $this->client->delete("invoices/{$id}");
    }

    /**
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($body)
    {
        return $this->client->post("invoices", ['form_params' => $body]);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function statusHistory($id)
    {
        return $this->client->get("invoices/{$id}/status");
    }

    /**
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function cost($body)
    {
        return $this->client->post("invoices/cost", ['form_params' => $body]);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getPdfSticker($id)
    {
        return $this->client->get("invoices/{$id}/pdf-sticker", ['headers' => ['Content-Type' => 'application/pdf']]);
    }
}