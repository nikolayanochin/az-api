<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-02-19
 */

namespace AzExpressTeam\Actions;

use GuzzleHttp\Client;
use ReflectionClass;

/**
 * Class BaseAction
 * @package AzExpressTeam\Actions
 */
class BaseAction
{
    /** @var Client */
    protected $client;

    /**
     * BaseAction constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $params
     * @return string
     * @throws \ReflectionException
     */
    public function getSearchString($params)
    {
        $reflect = new ReflectionClass($this);
        $query_string = '?';
        foreach ($params as $param => $value) {
            $query_string .= $reflect->getShortName() . 'Search[' . $param . ']=' . $value . '&';
        }

        return $query_string;
    }
}