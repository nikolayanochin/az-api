<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-04-15
 */

namespace AzExpressTeam\Actions;


use GuzzleHttp\Client;

/**
 * Class Pickup
 * @package AzExpressTeam\Actions
 */
class Pickup extends BaseAction
{
    /** @var Client */
    protected $client;


    const TYPE_DEFAULT = 1;
    const TYPE_WITH_SKU = 2;

    const STATUS_NEW = 0;
    const STATUS_WAIT = 1;
    const STATUS_PROGRESS = 2;
    const STATUS_ACCEPTED = 3;
    const STATUS_CANCELED = 4;
    /**
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \ReflectionException
     */
    public function getList($params = [])
    {
        $query = $this->getSearchString($params);
        return $this->client->get("pickup{$query}");
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getView($id)
    {
        return $this->client->get("pickup/{$id}");
    }

    /**
     * @param $id
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update($id, $body)
    {
        return $this->client->put("pickup/{$id}", ['form_params' => $body]);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($id)
    {
        return $this->client->delete("pickup/{$id}");
    }

    /**
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($body)
    {
        return $this->client->post("pickup", ['form_params' => $body]);
    }

}