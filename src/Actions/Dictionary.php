<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-02-20
 */

namespace AzExpressTeam\Actions;

use GuzzleHttp\Client;

/**
 * Class Dictionary
 * @package AzExpressTeam\Actions
 */
class Dictionary extends BaseAction
{
    /** @var Client */
    protected $client;

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getStatusesList()
    {
        return $this->client->get("dictionary/statuses");
    }

}