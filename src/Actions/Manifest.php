<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-02-20
 */

namespace AzExpressTeam\Actions;


use GuzzleHttp\Client;

/**
 * Class Manifest
 * @package AzExpressTeam\Actions
 */
class Manifest extends BaseAction
{
    /** @var Client */
    protected $client;


    const TYPE_COMING = 1;
    const TYPE_EXPENSE = 2;

    const STATUS_NEW = 0;
    const STATUS_WAIT = 1;
    const STATUS_INCOMPLETE = 2;
    const STATUS_APPROVE = 3;

    /**
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \ReflectionException
     */
    public function getList($params = [])
    {
        $query = $this->getSearchString($params);
        return $this->client->get("manifests{$query}");
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getView($id)
    {
        return $this->client->get("manifests/{$id}");
    }

    /**
     * @param $id
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function update($id, $body)
    {
        return $this->client->put("manifests/{$id}", ['form_params' => $body]);
    }

    /**
     * @param $id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete($id)
    {
        return $this->client->delete("manifests/{$id}");
    }

    /**
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create($body)
    {
        return $this->client->post("manifests", ['form_params' => $body]);
    }

}