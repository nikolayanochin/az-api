<?php
/**
 * Created with love by AzExpressTeam.
 * Author: Anochin Nikolay
 * Date: 2019-02-19
 */


namespace AzExpressTeam;

use AzExpressTeam\Actions\Dictionary;
use AzExpressTeam\Actions\Invoice;
use AzExpressTeam\Actions\Manifest;
use AzExpressTeam\Actions\Pickup;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;

/**
 * Class Api
 * @package AzExpressTeam
 */
class Api
{
    private $config_secret_key;
    private $config_url = 'https://az.express/api/';
    /** @var Client */
    private $client;

    /** @var Invoice */
    private $invoice;
    /** @var Dictionary */
    private $dictionary;
    /** @var Manifest */
    private $manifest;
    /** @var Pickup */
    private $pickup;


    /**
     * Api constructor.
     * @param $configs
     * @throws \Exception
     */
    public function __construct($configs)
    {
        foreach ($configs as $config_name => $value) {
            $config_name = 'config_' . $config_name;
            $this->$config_name = $value;
        }

        if (empty($this->config_secret_key)) {
            throw new \Exception('Invalid secret_key!');
        }

        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());

        $this->client = new Client([
            'base_uri' => $this->config_url,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->config_secret_key
            ]
        ]);
    }

    /**
     * @return Invoice
     */
    public function invoice(): Invoice
    {
        if (!$this->invoice) {
            $this->invoice = new Invoice($this->client);
        }

        return $this->invoice;
    }

    /**
     * @return Dictionary
     */
    public function dictionary(): Dictionary
    {
        if (!$this->dictionary) {
            $this->dictionary = new Dictionary($this->client);
        }

        return $this->dictionary;
    }

    /**
     * @return Manifest
     */
    public function manifest(): Manifest
    {
        if (!$this->manifest) {
            $this->manifest = new Manifest($this->client);
        }

        return $this->manifest;
    }

    /**
     * @return Pickup
     */
    public function pickup(): Pickup
    {
        if (!$this->pickup) {
            $this->pickup = new Pickup($this->client);
        }

        return $this->pickup;
    }


}